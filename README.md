# Breadth First Search (Recursive)

Breadth First Search(BFS) is an algorithm for traversing a graph where the neighbours of the starting node are visited followed by neighbours of neighbours and so on till the entire graph is explored. The source code is optimized with tail call recursion. The time complexity of BFS `O(|V|+|E|)` where `V` is the number of vertices and `E` is the number of edges.

For example the graph shown by `Map(0->List(1,2),1->List(0,3,4),2->List(0,1),3->List(1,5),4->List(1),5->List(3,6,7,8),6->List(5),7->List(5,8),8->List(5,7,9),9->List(8))` is:
![Graph](./graph.jpg "Graph")

Starting from `0` the nodes at a distance of 1 from it are `1` and `2`. The next nodes visited will be `3` and `4` as these are connected to `1` which is connected to `0` and both are at a distance of 2. This repeats untill the entire graph is traversed.

The algorigthm first queues all the nodes connected to the starting node and adds them to the list of visited nodes in the next pass and then queues up all the nodes which have just been added.

For the given graph, starting from `0`:

**Function Call:**

visited: `None`, queued:`List(0)`

**First call:**

visited: `List(0)`, queued:`List(1,2)`

**Second call:**

visited: `List(0,1,2)`, queued: `List(3,4)`

**Third call:**

visited: `List(0, 1, 2, 3, 4)`, queued: `List(5)`

**Fourth call:**

visited: `List(0, 1, 2, 3, 4, 5)`, queued: `List(6, 7, 8)`

**Fifth call**

visited: `List(0, 1, 2, 3, 4, 5, 6, 7, 8)`, queued: `List(9)`

**Sixth Call**

visited: `List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)`, queued: `None`

As the queue is empty, the visited list is returned. Hence the order of traversal is: `0, 1, 2, 3, 4, 5, 6, 7, 8, 9`
