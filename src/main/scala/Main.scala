object Main
{
  def main(args:Array[String]):Unit=
  {
    val graph=Map("a"->List("b","c"),"b"->List("a","d"),"c"->List(),"d"->List());
    val traverse=Map(0->List(1,2),1->List(0,3,4),2->List(0,1),3->List(1,5),
      4->List(1),5->List(3,6,7,8),6->List(5),7->List(5,8),8->List(5,7,9),
      9->List(8));
    println(bfs(0,traverse));
  }
  def bfs[T](start:T,graph:Map[T,List[T]]):List[T]=
  {
    @annotation.tailrec
    def loop(visited:List[T],queue:List[T]):List[T]=
    {
      if(queue.length==0) visited;
      else
      {
       //Uncomment to see how the traversal goes
       //println(visited++queue,children(queue,graph).filter(x=>(visited++queue).contains(x)==false))
        loop(visited++queue,children(queue,graph).filter(x=>(visited++queue).contains(x)==false));
      }
    }
    loop(List[T](),List(start));
  }
  def children[T](parent:List[T],graph:Map[T,List[T]]):List[T]=
  {
    //Get the next level of the graph
    @annotation.tailrec
    def loop(i:Int,acc:List[T]):List[T]=
    {
      if(i==parent.length) acc.distinct;
      else
      {
        loop(i+1,acc++graph(parent(i)));
      }
    }
    loop(0,List[T]());
  }
}
